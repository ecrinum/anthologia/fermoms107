MS 107 (4 CA 2/107)

Prete, _I codici della biblioteca comunale di Fermo_, Olschki, 1960
p. 158: description du manuscrit

Opuscolo cartaceo XVIII-XIX, mm 130x100 di cc 28 riunite in un solo fascicolo; bianche le cc 19 e ss. Contiene una raccolta di epigrammi e poesie (Callimaco, Teocrito, Archiloco etc.) transcritti in greco, colla trad. italiana, a fronte, numero per numero. La raccolta, forse ad uso scolastico, non ha alcun valore.

Copertina di cartone, stato di conservazione buono.

Epigrammi greci cc. 1-18
nI Callimaco, epigr. VI (7), ed Pfeifer (Oxonii 1953): a Creofulo; n. 77 epigramma di Pallada, cfr. J Hutton, p 238 

Bibliografia: raffaelle, voce Callimaco e Hutton

Raffaelli è il marchese che ha fattpo il primo catalogo


Hutton, The greek anthology in italy to the year 1800, New York 1935 - cornell studies 23

https://www.persee.fr/doc/rht_0373-6075_1982_num_10_1980_1214


## Contenu

Carton pour couverture. Feuilles papier avec tissu 4 colonne 2 cm |----|----|----|---|

centre reliure feuillet 17

coupés entre 27 et 28 4 feuilles (correspondants à 2,3,4)

### 2e couve:

E-% écrit crayon

4CA2 <!--encre brun--, raturé-->
n 107

C II. (sur papier imprimé décollé)

### Feuille 1 recto: vide

### 1v trad italiana:

Sopra Creufulo. Epigramma di Callimaco

1. Io son lavoro di uno di Samo, il quale accolse 
### 2r Greco:
εισ κρεοφυλον καλλιμακυ

Ensuite épigrammes numérottées:

1: Cf Edition de Pfeifer https://archive.org/details/callimachus0000call/page/80/mode/2up (p 82 VI(7))


2: cf https://archive.org/details/callimachihymni00hensgoog/page/280/mode/2up L'éditeur ici (1761) dit: "Ex anthologia liber I c 50 

3: 7.520

4: 7.447

5: 7.524

6: 7.453 

7: 7.525 

### 3r

fin 7

8: 7.518 

9: 7.272
10: 7.271
11: 6.347

12: 6.351

### 4r

13: 7.451 

14: 7.320 - 2e couplet (le premier est absent): attribué à callimaque dans le ms 107, Egesippo dans notre plateforme - ici attribué à callimaque: https://archive.org/details/callimachihymni00hensgoog/page/276/mode/2up
15: 7.317

Anacreonte:
16: 7.160 

17: 7.226

18: 9.715

19: 9.716

### 5r

Saffo

20: 7.505 

Teocrito

21: 6.336

22: 9.432 


23: 7.658 

Simonide

24:  ΕΛΛΗΝΩΝ ΑΡΧΗΓΟΣ ΕΠΕΙ ΣΤΡΑΤΟΝ ΩΛΕΣΕ ΜΗΔΩΝ,

ΠΑΥΣΑΝΙΑΣ ΦΟΙΒΩ ΜΝΗΜ΄ ΑΝΕΘΗΚΕ ΤΟΔΕ. ??

### 6r
Archiloco
25:  6.133

Asclepiade

26: ??

Menandro

27: 7.72

Simonide

28: O xein... 7.249 

Platone

29: 9.506  (il y a une variation d'antipater!, 9.66)


## 7r

Polliano

32: 11.127

Adriano

35: 9.387

Demodoco

36: 11.237 


## 8v - cambia mano? it sul r grec v

43: ripete il 10! 7.271 traduzione diversa
